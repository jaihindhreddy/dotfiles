# .bashrc

# If not running interactively, don't do anything
# [[ $- != *i* ]] && return

# To enable smooth scrolling on firefox with X11
export MOZ_USE_XINPUT2=1

alias ls='ls --color=auto'
alias feh='feh --scale-down --auto-zoom'

# To set brightness
alias brightness='sudo tee /sys/class/backlight/intel_backlight/brightness <<< '

# Setting  bash prompt
export PS1='\W  👉 '

# To view YouTube videos in mpv
alias yy="mpv --really-quiet --volume=50 --autofit=30% --geometry=-10-15 --ytdl --ytdl-format='mp4[height<=?1080]' -ytdl-raw-options=playlist-start=1"
alias update='sudo xbps-install -Su'

# Adding JDownloader to the PATH
export PATH=$PATH:jd2
export PATH=$PATH:~/.scripts

# Setting default editor
export EDITOR=vim
export BROWSER=firefox
export XDG_CONFIG_HOME=~/.config

alias cfgbash='vim ~/.bashrc'
alias cfgi3='vim ~/.config/i3/config'
alias cfgranger='vim ~/.config/ranger/rc.conf'
alias cfgmpd='vim ~/.config/mpd/mpd.conf'
alias cfgdunst='vim ~/.config/dunst/dunstrc'
alias cfgncmpcpp='vim ~/.config/ncmpcpp/config'
alias cfgurxvt='vim ~/.config/urxvtconfig/config'

alias ll='ls -al'
alias config='/usr/bin/git --git-dir=/home/jaihindhreddy/.cfg_git/ --work-tree=/home/jaihindhreddy'
